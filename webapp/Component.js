jQuery.sap.declare("com.demo.s276.ALPDemo2.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
sap.ui.define(["sap/ui/generic/app/AppComponent"], function (AppComponent) {
	return AppComponent.extend("com.demo.s276.ALPDemo2.Component", {
		metadata: {
			"manifest": "json"
		}
	});
});